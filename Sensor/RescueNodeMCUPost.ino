#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>

#define USE_SERIAL Serial

ESP8266WiFiMulti WiFiMulti;

int buttonState = 0; 
int incLat = 1070651457;
int incLog =  1071375620;
int buttonPin = 5; 

void setup() {

    USE_SERIAL.begin(57600);
    USE_SERIAL.println();
    USE_SERIAL.println();
    USE_SERIAL.println();

    for(uint8_t t = 4; t > 0; t--) {
        USE_SERIAL.printf("[SETUP] WAIT %d...\n", t);
        USE_SERIAL.flush();
        delay(1000);
    }

    WiFiMulti.addAP("Eu", "neken123");

}

void loop() {
    // wait for WiFi connection
    buttonState = digitalRead(buttonPin);
    if((WiFiMulti.run() == WL_CONNECTED)) {

        HTTPClient http;

        USE_SERIAL.print("[HTTP] begin...\n");
  
    if (buttonState == HIGH) {
         
        String mensagem = "";
        char mensagemC[100];
        mensagem = "[{\"CustomerId\":3,\"latitude\":\"-23."+String(incLat)+"\",\"longitude\":\"-46."+String(incLog)+"\"}]";
        incLat  += 10000;
        incLog  +=  10000;

 

 
        mensagem.toCharArray(mensagemC,mensagem.length()+1);
  
        http.begin("http://52.67.109.190/home/SendData?responseString="+mensagem); //HTTP

        USE_SERIAL.print("[HTTP] GET...\n");
        // start connection and send HTTP header
        int httpCode = http.GET();

        // httpCode will be negative on error
        if(httpCode > 0) {
            // HTTP header has been send and Server response header has been handled
            USE_SERIAL.printf("[HTTP] GET... code: %d\n", httpCode);

            // file found at server
            if(httpCode == HTTP_CODE_OK) {
                String payload = http.getString();
                USE_SERIAL.println(payload);
            }
        } else {
            USE_SERIAL.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
        }

        http.end();
    }

    delay(10000);
    }
}

