/*
Exemplo básico de conexão a Konker Plataform via MQTT, baseado no https://github.com/knolleary/pubsubclient/blob/master/examples/mqtt_auth/mqtt_auth.ino. Este exemplo se utiliza das bibliotecas do ESP8266 programado via Arduino IDE (https://github.com/esp8266/Arduino) e a biblioteca PubSubClient que pode ser obtida em: https://github.com/knolleary/pubsubclient/
*/


#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// Vamos primeiramente conectar o ESP8266 com a rede Wireless (mude os parâmetros abaixo para sua rede).

const char* ssid = "Eu";
const char* password = "neken123";
const int buttonPin = 5; 

//Criando a função de callback
void callback(char* topic, byte* payload, unsigned int length) {
  // Essa função trata das mensagens que são recebidas no tópico no qual o Arduino esta inscrito.
}

//Criando os objetos de conexão com a rede e com o servidor MQTT.
WiFiClient espClient;
PubSubClient client("mqtt.hackathon.konkerlabs.net", 1883, callback,espClient);
ADC_MODE(ADC_VCC);
char mensagemC[100];
int buttonState = 0; 
String mensagem ="";
int incLat = 5874113;
int incLog = 6598276;

void setup()
{
  //Conectando na Rede
    
    
    Serial.begin(57600);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) 
      {
        delay(500);
        Serial.print(".");
      }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());

    
    
   
  
}

void loop()
{
  
  if (client.connect("arduinoClient", "17q2nupnf5cl", "k1q78DIgRpBw")) {
    buttonState = digitalRead(buttonPin);
    incLat += 10000;
    incLog  +=  10000;
    mensagem = "{\"CustomerId\":3, \"latitude\":\"-23." + String(incLat) + "\", \"longitude\":\"-46." + String(incLog) + "\"}";
    mensagem.toCharArray(mensagemC,mensagem.length()+1);
    if (buttonState == HIGH) {
      
    client.publish("pub/17q2nupnf5cl/konker",mensagemC);
    Serial.println(mensagemC);
    }
  }
}
