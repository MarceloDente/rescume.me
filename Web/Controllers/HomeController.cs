﻿using help.me.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace help.me.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            _DbContext db = null;

            try
            {
                db = new _DbContext();

                //DEFINE OFFSET TO SEARCH NEW OCCURENCES
                double ticks = 0;

                Occurence lastOccurence = db.Occurences.OrderByDescending(o => o.Id).FirstOrDefault();

                if (lastOccurence != null)
                {
                    ticks = DateToTick(lastOccurence.Date);
                }

                ViewBag.Occurences = db.Occurences.Include("Customer.Company").OrderByDescending(o => o.Id).ToList();
            }
            catch (Exception ex)
            {
                ViewBag.Error = ex.Message;
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }

            return View();
        }

        [AllowAnonymous]
        [HttpGet]
        public JsonResult SendData(string responseString)
        {
            try
            {
                List<Occurence> ocs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Occurence>>(responseString);

                if (ocs.Count == 0)
                {
                    return Json(new { code = 100 }, JsonRequestBehavior.AllowGet);
                }

                string result = SendOcurrenceList(ocs);
                return Json(new { code = 200, result = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { erro = ex }, JsonRequestBehavior.AllowGet);
            }
        }

        private string SendOcurrenceList(List<Occurence> ocs)
        {
            _DbContext db = null;

            try
            {
                //NO DATA
                ocs = ocs.Where(w => w.CustomerId != 0 && !string.IsNullOrEmpty(w.latitude) && !string.IsNullOrEmpty(w.longitude)).ToList();

                if (ocs.Count == 0)
                {
                    return "Vazio";
                }

                //REMOVE DUPLICATES
                ocs = ocs.GroupBy(g => new { g.CustomerId, g.latitude, g.longitude }).Select(s => s.FirstOrDefault()).ToList();

                if (ocs.Count == 0)
                {
                    return "Vazio";
                }

                //REMOVE EXISTS    

                //var texistOccurences = db.Occurences;

                //if (texistOccurences != null && texistOccurences.Count() > 0)
                //{
                //List<Occurence> existOccurences = db.Occurences.ToList();

                //if (existOccurences != null)
                //{
                //    ocs = ocs.Where(w => !existOccurences.Any(a => a.CustomerId == w.CustomerId && a.latitude == w.latitude && a.longitude == w.longitude)).ToList();

                //    if (ocs.Count == 0)
                //    {
                //        return "Vazio";
                //    }
                //}
                ////}

                //NEW
                ocs.ForEach(oc =>
                {
                    oc.Date = DateTime.Now;

                    latlng ll = GetGeoDataDatas(oc.latitude, oc.longitude);

                    if (ll != null)
                    {
                        oc.latitude = ll.lat.ToString();
                        oc.longitude = ll.lng.ToString();
                    }

                });

                db = new _DbContext();
                db.Occurences.AddRange(ocs);
                db.SaveChanges();

                return "Sucesso";

            }
            catch (Exception ex)
            {
                return ex.Message + " - " + ((ex.InnerException == null) ? "" : ex.InnerException.Message + " - " + ((ex.InnerException.InnerException == null) ? "" : ex.InnerException.InnerException.Message));
            }
            finally
            {
                if (db != null)
                {
                    db.Dispose();
                }
            }
        }

        private latlng GetGeoDataDatas(string lat, string lgt)
        {
            try
            {
                string url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lgt + "&sensor=true_or_false";

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                GoogleGeoCode Result = Newtonsoft.Json.JsonConvert.DeserializeObject<GoogleGeoCode>(responseString);

                latlng ll = new latlng();


                Northeast n = Result.results.First().geometry.bounds.northeast;

                ll.lat = n.lat;
                ll.lng = n.lng;

                return ll;

            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return null;
            }
        }

        private static double DateToTick(DateTime date)
        {
            DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan ticks = date.ToUniversalTime() - start;
            return Math.Floor(ticks.TotalMilliseconds);
        }
    }
}
