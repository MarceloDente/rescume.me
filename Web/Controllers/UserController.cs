﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using help.me.Models;
using System.Web.Security;

namespace help.me.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private _DbContext db = new _DbContext();

        public ActionResult Index()
        {
            return View(db.Users.ToList());
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                ViewBag.PageName = "Novo Usuário";
                return View(new User());
            }
            User user = db.Users.Find(id);
            if (user != null)
            {
                ViewBag.PageName = "Editar " + user.FirstName + " " + user.LastName;
                return View(user);
            }

            return HttpNotFound();            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,FirstName,LastName,CPF,Email,Password")] User user)
        {
            if (ModelState.IsValid)
            {
                if (user.Id == 0)
                {
                    db.Users.Add(user);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View(user);
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login([Bind(Include = "Email,Password")] User user)
        {
            User login = db.Users.FirstOrDefault(u => u.Email == user.Email && u.Password == user.Password);

            if (login != null)
            {
                FormsAuthentication.RedirectFromLoginPage(user.Id.ToString(), false);
                return RedirectToAction("../Home");
            }

            return View();
        }

        public ActionResult Logout()
        {
            Session.RemoveAll();
            FormsAuthentication.SignOut();
            return Redirect("/");
        }

        public ActionResult Delete(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
