﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using help.me.Models;

namespace help.me.Controllers
{
    [Authorize]
    public class CompanyController : Controller
    {
        private _DbContext db = new _DbContext();

        public ActionResult Index()
        {
            return View(db.Companies.ToList());
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                ViewBag.PageName = "Nova Empresa";
                return View(new Company());
            }
            Company company = db.Companies.Find(id);
            if (company != null)
            {
                ViewBag.PageName = "Editar " + company.CompanyName;
                return View(company);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,CompanyName,CNPJ,HashAccess")] Company company)
        {
            if (ModelState.IsValid)
            {
                if (company.Id == 0)
                {
                    var base64 = System.Text.Encoding.UTF8.GetBytes(company.HashAccess);
                    company.HashAccess = System.Convert.ToBase64String(base64);

                    db.Companies.Add(company);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    db.Entry(company).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View(company);
        }

        public ActionResult Delete(int id)
        {
            try
            {
                Company company = db.Companies.Find(id);
                db.Companies.Remove(company);
                db.SaveChanges();
                return RedirectToAction("Index");
            } catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
