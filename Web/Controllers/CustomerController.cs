﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using help.me.Models;

namespace help.me.Controllers
{
    [Authorize]
    public class CustomerController : Controller
    {
        private _DbContext db = new _DbContext();

        public ActionResult Index()
        {
            var customers = db.Customers.Include(c => c.Company);
            return View(customers.ToList());
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                ViewBag.PageName = "Novo Cliente";
                ViewBag.CompanyID = new SelectList(db.Companies, "Id", "CompanyName");
                return View(new Customer());
            }
            Customer customer = db.Customers.Find(id);
            if (customer != null)
            {
                ViewBag.PageName = "Editar " + customer.FirstName + " " + customer.LastName;
                ViewBag.CompanyID = new SelectList(db.Companies, "Id", "CompanyName", customer.CompanyID);
                return View(customer);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,CPF,Phone,Email,Vehicle,BoardVehicle,Renavam,Year,EquipamentID,ContactName1,ContactPhone1,ContactName2,ContactPhone2,ContactName3,ContactPhone3,HashAccess,CompanyID")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                if (customer.Id == 0)
                {
                    db.Customers.Add(customer);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    db.Entry(customer).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            ViewBag.CompanyID = new SelectList(db.Companies, "Id", "CompanyName", customer.CompanyID);
            return View(customer);
        }

        public ActionResult Delete(int id)
        {
            Customer customer = db.Customers.Find(id);
            db.Customers.Remove(customer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
