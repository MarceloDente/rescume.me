﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace help.me.Models
{
    [Table("Occurences")]
    public class Occurence
    {
        public int Id { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        public DateTime Date { get; set; }
    }
}