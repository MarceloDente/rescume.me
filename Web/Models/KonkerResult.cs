﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace help.me.Models
{
    [NotMapped]
    public class KonkerResult
    {
        public Meta meta { get; set; }
        public Occurence data { get; set; }
    }

    [NotMapped]
    public class Incoming
    {
        public string deviceId { get; set; }
        public string channel { get; set; }
    }

    [NotMapped]
    public class Outgoing
    {
        public string deviceId { get; set; }
        public string channel { get; set; }
    }

    [NotMapped]
    public class Meta
    {
        public long timestamp { get; set; }
        public Incoming incoming { get; set; }
        public Outgoing outgoing { get; set; }
    }
}