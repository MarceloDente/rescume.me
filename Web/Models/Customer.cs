﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace help.me.Models
{
    [Table("Customers")]
    public class Customer
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string CPF { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string Vehicle { get; set; }

        public string BoardVehicle { get; set; }

        public string Renavam { get; set; }

        public int? Year { get; set; }

        public string EquipamentID { get; set; }

        public string ContactName1 { get; set; }

        public string ContactPhone1 { get; set; }

        public string ContactName2 { get; set; }

        public string ContactPhone2 { get; set; }

        public string ContactName3 { get; set; }

        public string ContactPhone3 { get; set; }

        public string HashAccess { get; set; }

        public int? CompanyID { get; set; }

        public virtual Company Company { get; set; }
    }
}
