﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace help.me.Models
{
    [Table("Companies")]
    public class Company
    {
        public int Id { get; set; }

        public string CompanyName { get; set; }

        public string CNPJ { get; set; }

        public string HashAccess { get; set; }
    }
}