﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;

namespace help.me.Models
{
    public class MyConfiguration : System.Data.Entity.Migrations.DbMigrationsConfiguration<_DbContext>
    {
        public MyConfiguration()
        {
            this.AutomaticMigrationsEnabled = true;
            this.AutomaticMigrationDataLossAllowed = true;
        }
    }

    public class _DbContext : DbContext
    {
        public DbSet<Company> Companies { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Occurence> Occurences { get; set; }
        public DbSet<User> Users { get; set; }

        public _DbContext() : base("name=DBContext")
        {
            SetInitializer();
        }

        private void SetInitializer()
        {
            //Database.SetInitializer<_DataBaseContext>(null);
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<_DbContext, MyConfiguration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

    }
}