﻿var geoloc;
var fat = 1;
var segs = [];
var limps = [];
var segCor = { 'marker-color': '#f86767' };
var limpCor = { 'marker-color': '#' };
var segQnt = 10;
var limpQnt = 5;
var map;

if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (e) {
        geoloc = [e.coords.latitude, e.coords.longitude];
        loadMap(geoloc);
    },
    function () {
        geoloc = [-23.550138, -46.633412];
        loadMap(geoloc);
    });
}

function loadMap(geoloc) {
    L.mapbox.accessToken = 'pk.eyJ1Ijoic21hcnRwYXJrcyIsImEiOiJjaXViajNlZzAwMDV1MzRxZjhueWc2dDN2In0.y6xRq4ygnplxXn0KwU2LFA';
    map = L.mapbox.map('map', 'mapbox.streets');
    map.setView(geoloc, 12);

    for (var x = 0; x < data.length; x++) {
        var obj = L.marker([data[x].latitude,data[x].longitude], {
            icon: L.mapbox.marker.icon(segCor),
            title: data[x].Customer.CPF,
        });
        obj.bindPopup("<p>ALGO ACONTECEU COM O VEÍCULO DE " + data[x].Customer.FirstName + "  " + data[x].Customer.LastName + " "+ data[x].Date+" </p>");
        obj.togglePopup();
        obj.addTo(map);
    }

    map.setView([data[0].latitude, data[0].longitude]);
}

$("[car-data]").click(putInMap);
function putInMap(e) {
    var data = $(this).attr("car-data");
    $(this).attr("disabled",true);
    data = JSON.parse(data);
    var obj = L.marker([data.latitude, data.longitude], {
        icon: L.mapbox.marker.icon(segCor),
        title: data.CPF,
    });
    obj.bindPopup("<p>ALGO ACONTECEU COM O VEÍCULO DE " + data.FirstName + "  " + data.LastName + " " + data.Date + " </p>");
    obj.togglePopup();
    obj.addTo(map);
    map.setView([data.latitude, data.longitude]);
}
