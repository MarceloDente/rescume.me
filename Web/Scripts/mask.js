﻿(function ($) {
    'use strict';

    $("form").submit(function (event) {
        //remove mascaras para realizar o submit
        $('#CPF').unmask('000.000.000-00');
        $('#Phone').unmask('(00)00000-0000');
        $('#ContactPhone1').unmask('(00)00000-0000');
        $('#ContactPhone2').unmask('(00)00000-0000');
        $('#ContactPhone3').unmask('(00)00000-0000');
        $('#CNPJ').unmask('00.000.000/0000-00');
    });
})(jQuery);

$(document).ready(function () {
    //coloca mascaras
    $('#CPF').mask('000.000.000-00');
    $('#Phone').mask('(00)00000-0000');
    $('#ContactPhone1').mask('(00)00000-0000');
    $('#ContactPhone2').mask('(00)00000-0000');
    $('#ContactPhone3').mask('(00)00000-0000');
    $('#CNPJ').mask('00.000.000/0000-00');
});